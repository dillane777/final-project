import { Routes, Route } from 'react-router-dom';
import { Home } from '../../modules/home'
import { About } from '../../modules/about'
import { Portfolio } from '../../modules/portfolio'
import { Testimonials } from '../../modules/testimonials/Testimonials';
import { Contact } from '../../modules/contact'

export const AppRoutes = () => {
    return(
        <Routes>
            <Route index element={<Home />}/>
            <Route path='/about/*' element={<About />}/>
            <Route path='/portfolio/*' element={<Portfolio />}/>
            <Route path='/testimonials/*' element={<Testimonials />} />
            <Route path='/contact/*' element={<Contact />}/>
        </Routes>
    )
}