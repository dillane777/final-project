import './LayOutContainer.css'

export const LayOutContainer = ({children, className}) => {
    return (
        <div className={className ? `${className} container`: 'container' }>
            {children}
        </div>
    )
}