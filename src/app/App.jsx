import { Nav } from './../modules/nav/Nav'
import { AppRoutes } from './components/AppRoutes'
import { LayOutContainer } from './components/LayOutContainer'
import './App.css';
import './media.css';
import { Footer } from '../modules/footer/Footer';


export const App = () => {
       return (
        <>
            <div className='site'>
            <header className="header">
                <LayOutContainer className='header__wrapper'>
                    <Nav />                                 
                </LayOutContainer>
            </header>
            <main>
                <LayOutContainer className='main__wrapper'>
                    <AppRoutes />
                </LayOutContainer>
            </main>
            <footer>
                <LayOutContainer className='footer__wrapper'>
                    <Footer />
                </LayOutContainer>
            </footer>
            </div>
     </>
    )
}