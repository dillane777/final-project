import aboutImg from './../../utils/images/portfolio__7.png'

export const About = () => {
    return (
        <>        
        <h2>About</h2>
        <div className="about__wrapper">
            <p className="about__text">We create stunning Lorem ipsum dolor sit amet, consectetur adipiscing elit.We create stunning Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            <br/>
            <br/>
            After 10 years of experience Lorem ipsum dolor sit amet, consectetur adipiscing elit.We create stunning Lorem ipsum dolor sit amet, consectetur adipiscing elit.We create stunning Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

            <img className="about__img" src={aboutImg} alt=""/>
        </div>
        </>
    )
}