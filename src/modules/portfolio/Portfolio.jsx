import imgOne from './../../utils/images/portfolio__1.png'
import imgTwo from './../../utils/images/portfolio__2.png'
import imgThree from './../../utils/images/portfolio__3.png'
import imgFour from './../../utils/images/portfolio__4.png'
import imgFive from './../../utils/images/portfolio__5.png'
import imgSix from './../../utils/images/portfolio__6.png'
import { Carousel } from 'antd';


export const Portfolio = () => {           
        
          return (
            <>
            <h2>Portfolio</h2>
            <Carousel autoplay>
              <div>
              <img className='img__one' src={imgOne} alt=''/>
              </div>
              <div>
              <img className='img__two' src={imgTwo} alt=''/>
              </div>
              <div>
              <img className='img__three' src={imgThree} alt=''/>
              </div>
              <div>
              <img className='img__four' src={imgFour} alt=''/>
              </div>
              <div>
              <img className='img__five' src={imgFive} alt=''/>
              </div>
              <div>
              <img className='img__six' src={imgSix} alt=''/>
              </div>
            </Carousel>
            </>
          );
    
}