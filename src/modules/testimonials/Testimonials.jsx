import { Routes, Route } from "react-router-dom"
import { TestimonialList } from "./testimonialList/TestimonialList"

export const Testimonials = () => {
    return (
        <Routes>
            <Route index element={<TestimonialList />}/>
        </Routes>
        )
}