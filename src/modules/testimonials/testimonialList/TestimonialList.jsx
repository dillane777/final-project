import testImgOne from './../../../utils/images/testimonial__1.png'
import testImgTwo from './../../../utils/images/testimonial__2.png'
import testImgThree from './../../../utils/images/testimonial__3.png'


export const TestimonialList = () => {
        return (
        <>
        <h2>Testimonials</h2>
        <div className='testimonials__wrapper'>
        <p className='testimonials__text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <div className='reviews__wrapper'>
            <div className='left__col'>
            <div className='review__one'>
                <img className='review__img__one' src={testImgOne} alt=''/>
                <div className='review__content__one'>
                <h3 className='review__title__one'>“Amazing Team with Lorem Ipsum”</h3>
                <p className='review__text__one'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.</p>
                </div>
            </div>
            <div className='review__two'>                
                <div className='review__content__two'>
                <h3 className='review__title__two'>“Big Dreams for lorem ipsum”</h3>
                <p className='review__text__two'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.</p>
                </div>
                <img className='review__img__two' src={testImgTwo} alt=''/>
            </div>
            </div>
            <div className='review__three'>
                <img className='review__img__three' src={testImgThree} alt=''/>
                <div className='review__content__three'>
                <h3 className='review__title__three'>“Good Dreams for lorem ipsum”</h3>
                <p className='review__text__three'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.</p>
                </div>
            </div>
        </div>
        </div>
        </>

    )
}

