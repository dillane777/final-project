import { Link } from 'react-router-dom'
export const Home = () => {
    return (
    <>
    <div className='home__wrapper'>      
    <p className='home__text'>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Lorem, ipsum dolor sit amet consectetur adipisicing elit.
    </p>
    <h1 className='creative'>Creative</h1>
    <h1 className='design'>Design</h1>            
    <Link to="/portfolio" className='home__btn'>See more</Link>
    </div>
    </>
    )
}