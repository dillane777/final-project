import { Terms } from './terms/Terms';
import { PrivacyPolicy } from './privacyPolicy/PrivacyPolicy';

export const Footer = () => {
        
    return (
        <>
        <div className='footer__title'>2023 Company</div>
            <div className='footer__links'>
                <PrivacyPolicy />
                <Terms />                
            </div>
        </>
    )
}