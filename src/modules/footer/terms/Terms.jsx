import { Button, Modal } from 'antd'
import { useState } from 'react'


export const Terms = () => {
    
    const [isModalOpen, setIsModalOpen] = useState(false);

        const showModal = () => {
            setIsModalOpen(true);
        };

        const handleOk = () => {
            setIsModalOpen(false);
        };

        const handleCancel = () => {
            setIsModalOpen(false);
        };


    return (
        <>
        <Button type='link' onClick={showModal}>Terms and Conditions</Button>
            <Modal title="Terms and Conditions" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <p className='terms'>Welcome to creativedesign.com!
                <br/>                
                These terms and conditions outline the rules and regulations for the use of Creative Design's Website, located at www.creativedesign.com.
                <br/>
                By accessing this website we assume you accept these terms and conditions. Do not continue to use creativedesign.com if you do not agree to take all of the terms and conditions stated on this page.
                <br/>
                The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company's terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client's needs in respect of provision of the Company's stated services, in accordance with and subject to, prevailing law of us. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>
            </Modal>
        </>
    )
}