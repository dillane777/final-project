import { Button, Modal } from 'antd'
import { useState } from 'react'

export const PrivacyPolicy = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);

        const showModal = () => {
            setIsModalOpen(true);
        };

        const handleOk = () => {
            setIsModalOpen(false);
        };

        const handleCancel = () => {
            setIsModalOpen(false);
        };

    return (
        <>        
            <Button type='link' onClick={showModal}>Privacy Policy</Button>
            <Modal className='modal' title="Privacy Policy" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <p className='policy'>At creativedesign.com, accessible from www.creativedesign.com, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by creativedesign.com and how we use it.
                    <br/>
                    If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us.
                    <br/>
                    This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in creativedesign.com. This policy is not applicable to any information collected offline or via channels other than this website.
                    <br/>
                    By using our website, you hereby consent to our Privacy Policy and agree to its terms.</p>
            </Modal>
        </>
    )
}