import { Result } from 'antd'; 
import { useLocation, useNavigate } from 'react-router-dom'


export const ResultPage = () => {
    const { state } = useLocation()    
    const navigate = useNavigate();    
    const  { userName, email, message } = state;
    
    const handleGoHomeClick = () => {
        navigate('../..')
    }   

    return ( 
        <Result className='result'
            status="success"
            title="Thanks for your feedback!"
            subTitle={`Dear ${userName}! We received your message "${message}". We'll contact you by your email ${email} as soon as possible.`}
            extra={[
            <button className='result__btn' onClick={handleGoHomeClick} type="primary" key="console">
                ← Home
            </button>            
            ]}
      />
    )    
}