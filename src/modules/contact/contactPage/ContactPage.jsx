import { useNavigate } from 'react-router-dom'
import { Form } from 'antd'
import { useState, useEffect } from 'react';


export const ContactPage = () => {
    const navigate = useNavigate();
    const [userName, setUserName] = useState('')
    const [email, setEmail] = useState('')
    const [message, setMessage] = useState('')     

    const user = { userName, email, message }

    useEffect(() => {        
        localStorage.setItem('user', JSON.stringify(user))
    }, [user])
   
    const handleFormFinish = (value) => {        
        navigate('./result', {
            state: value
        })        
    }

    return (
        <Form className='form' onFinish={handleFormFinish} >
        <span className="input__title">Name</span>
         <Form.Item 
                                                         
            name="userName"
            rules={[
                {
                required: true,
                message: 'Required field',
                },
                {
                 min: 3,
                 message: 'Name is too short'
                }
            ]}
            >
            <input className="input" value={userName} onChange={(e) => setUserName(e.target.value)} type="text" placeholder="John Doe"/>
        </Form.Item>
        <span className="input__title">Email</span>
        <Form.Item                       
            name="email"
            rules={[
                {
                type: 'email',
                message: 'Email is non-valid',
                },
            ]}
            >
            <input className='input' value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="johndoe01@gmail.com"/>
        </Form.Item>
        <span className="input__title">Message</span>
        <Form.Item                        
            name="message"
            rules={[
                {
                 type: 'text',
                 required: false
                },
            ]}
            >
            <input className="message" value={message} onChange={(e) => setMessage(e.target.value)} type='text' placeholder="Enter your message here..."/>
        </Form.Item>       
        <Form.Item>
        <button className="form__btn" type="submit">Send</button>
        </Form.Item>
    </Form>
    )
    
}