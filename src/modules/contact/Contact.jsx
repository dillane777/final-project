import { Routes, Route } from "react-router-dom"
import { ContactPage } from "./contactPage/ContactPage"
import { ResultPage } from "./resultPage/ResultPage"

export const Contact = () => {
    return (
        <Routes>
            <Route index element={<ContactPage />} />            
            <Route path="/result/*" element={<ResultPage />} />
        </Routes>
    )
}

   
