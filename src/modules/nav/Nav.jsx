import { Link } from 'react-router-dom';
import logo from './../../utils/images/logo.png'
import { slide as Menu } from "react-burger-menu"

export const Nav = () => {    

    return (        
        <>
        <Link to='/'>
            <img className='logo__img' src={logo} alt=''/>
        </Link>        

        <div className="nav">
            <div className="nav__items">
                <Link to="/" className="nav_item">Home</Link>
                <Link to="/about" className="nav_item">About</Link>
                <Link to="/portfolio" className="nav_item">Portfolio</Link>
                <Link to="/testimonials" className="nav_item">Testimonials</Link>
                <Link to="/contact" className="nav_item">Contact</Link>
            </div>

        <div className="burger__visible">
                <Menu right>
                    <Link to="/" className="menu_item">Home</Link>
                    <Link to="/about" className="menu_item">About</Link>
                    <Link to="/portfolio" className="menu_item">Portfolio</Link>
                    <Link to="/testimonials" className="menu_item">Testimonials</Link>
                    <Link to="/contact" className="menu_item">Contact</Link>
                </Menu>
        </div>
        </div>
                    
        </>
    )
}